==============
DynDNS Monitor
==============

A DNS updater application for Linux systems based on Lexicon_.

Usage
=====

.. code::

  usage: dyndns-monitor [-h] [--config-dir CONFIG_DIR] [-v] interface

  positional arguments:
    interface                the interface which is monitored for IP address changes

  optional arguments:
    -h, --help               show this help message and exit
    --config-dir CONFIG_DIR  specify the directory where to search lexicon.yml and
                             lexicon_[provider].yml configuration files (default:
                             current directory).
    -v, --verbosity          increase output verbosity

Configuration is done in the same way as Lexicon.  For information on this see
the `Lexicon configuration reference`_.

.. _Lexicon: https://github.com/AnalogJ/lexicon
.. _Lexicon configuration reference: https://dns-lexicon.readthedocs.io/en/latest/configuration_reference.html
