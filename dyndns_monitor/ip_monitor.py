import ipaddress

from pyroute2.netlink import NLM_F_REQUEST, NLM_F_DUMP, NLMSG_ERROR
from pyroute2.netlink.rtnl import RTMGRP_IPV4_IFADDR, RTMGRP_IPV6_IFADDR
from pyroute2.netlink.rtnl import RTM_GETLINK, RTM_GETADDR, RTM_NEWADDR, RTM_DELADDR
from pyroute2.netlink.rtnl.ifaddrmsg import ifaddrmsg, IFA_F_SECONDARY
from pyroute2.netlink.rtnl.ifinfmsg import ifinfmsg
from pyroute2.netlink.rtnl.iprsocket import IPRSocket


def _ip_to_rtype(ip):
    if isinstance(ip, ipaddress.IPv4Address):
        return "A"
    if isinstance(ip, ipaddress.IPv6Address):
        return "AAAA"
    return None


class IPMonitor:
    def __init__(self, interface, *, new_callback=None, del_callback=None):
        groups = RTMGRP_IPV4_IFADDR | RTMGRP_IPV6_IFADDR

        self._sock = IPRSocket()
        self._sock.bind(groups=groups)

        self._callbacks = {
            RTM_NEWADDR: new_callback,
            RTM_DELADDR: del_callback,
        }

        packets = self._sock.nlm_request(
            ifinfmsg(),
            msg_type=RTM_GETLINK,
            msg_flags=NLM_F_REQUEST | NLM_F_DUMP,
            terminate=lambda x: x["header"]["type"] == NLMSG_ERROR,
        )

        self._interface_index = None
        for packet in packets:
            for attr_name, attr_val in packet["attrs"]:
                if attr_name == "IFLA_IFNAME" and attr_val == interface:
                    self._interface_index = packet["index"]
        if self._interface_index is None:
            raise Exception('Interface "{interface}" not found')

    def _process_packet(self, packet):
        callback = self._callbacks.get(packet["header"]["type"])
        if callback is None:
            return

        if packet["index"] != self._interface_index:
            return

        ip = None
        temporary = False
        for attr_name, attr_val in packet["attrs"]:
            if attr_name == "IFA_ADDRESS":
                ip = attr_val
            if attr_name == "IFA_FLAGS":
                temporary = (attr_val & IFA_F_SECONDARY) != 0
        if ip is None:
            return

        if temporary:
            return

        processed_ip = ipaddress.ip_address(ip)
        if not processed_ip.is_global:
            return

        rtype = _ip_to_rtype(processed_ip)
        if rtype is None:
            return

        callback(rtype, ip)

    def run(self):
        packets = self._sock.nlm_request(
            ifaddrmsg(),
            msg_type=RTM_GETADDR,
            msg_flags=NLM_F_REQUEST | NLM_F_DUMP,
            terminate=lambda x: x["header"]["type"] == NLMSG_ERROR,
        )
        for packet in packets:
            self._process_packet(packet)

        while True:
            for packet in self._sock.get():
                self._process_packet(packet)
