import argparse
import logging
import os

from lexicon.config import ConfigResolver as LexiconConfigResolver
from lexicon.client import Client as LexiconClient
from requests.exceptions import HTTPError

try:
    from systemd.journal import JournalHandler

    HAS_SYSTEMD = True
except ImportError:
    HAS_SYSTEMD = False

from dyndns_monitor import IPMonitor

LOGGER = logging.getLogger()


def fd_is_journal(fd):
    journal_stream = os.environ.get("JOURNAL_STREAM")
    if journal_stream is None:
        return False

    journal_stream = journal_stream.split(":")
    if len(journal_stream) != 2:
        return False

    try:
        dev = int(journal_stream[0])
        ino = int(journal_stream[1])
    except ValueError:
        return False

    fd_info = os.fstat(fd)

    return fd_info.st_dev == dev and fd_info.st_ino == ino


def handle_new_ip(provider, name, domain):
    def inner_func(rtype, content):
        LOGGER.info(
            'Creating RR: "%s.%s." "%s" %s', name, domain, rtype, content
        )
        try:
            provider.create_record(rtype=rtype, name=name, content=content)
        except HTTPError as err:
            LOGGER.error(err)

    return inner_func


def handle_del_ip(provider, name, domain):
    def inner_func(rtype, content):
        LOGGER.info(
            'Deleting RR: "%s.%s." "%s" %s', name, domain, rtype, content
        )
        try:
            provider.delete_record(rtype=rtype, name=name, content=content)
        except HTTPError as err:
            LOGGER.error(err)

    return inner_func


def main():
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(logging.Formatter(fmt="%(levelname)s: %(message)s"))
    if HAS_SYSTEMD and fd_is_journal(2):
        log_handler = JournalHandler(SYSLOG_IDENTIFIER="dyndns-monitor")
    LOGGER.addHandler(log_handler)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--config-dir",
        default=os.getcwd(),
        help=(
            "specify the directory where to search lexicon.yml and lexicon_[provider].yml "
            "configuration files (default: current directory)."
        ),
    )
    parser.add_argument(
        "-v", "--verbosity", action="count", help="increase output verbosity", default=0
    )
    parser.add_argument(
        "interface",
        help="the interface which is monitored for IP address changes",
    )
    args = parser.parse_args()

    if args.verbosity >= 2:
        LOGGER.setLevel(logging.DEBUG)
    elif args.verbosity == 1:
        LOGGER.setLevel(logging.INFO)
    else:
        LOGGER.setLevel(logging.WARNING)

    config = LexiconConfigResolver()
    config.with_env().with_config_dir(args.config_dir)

    name = config.resolve("lexicon:name")
    domain = config.resolve("lexicon:domain")

    LOGGER.info("Interface: %s", args.interface)
    LOGGER.info("DNS record: %s.%s.", name, domain)

    with LexiconClient(config) as provider:
        del_ip_handler = handle_del_ip(provider, name, domain)

        # Remove any existing records
        del_ip_handler("A", None)
        del_ip_handler("AAAA", None)

        monitor = IPMonitor(
            args.interface,
            new_callback=handle_new_ip(provider, name, domain),
            del_callback=del_ip_handler,
        )
        try:
            monitor.run()
        except KeyboardInterrupt:
            LOGGER.info("Shutting down")

        del_ip_handler("A", None)
        del_ip_handler("AAAA", None)


if __name__ == "__main__":
    main()
